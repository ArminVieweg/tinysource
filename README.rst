tinysource for TYPO3 CMS (DEPRECATED!)
========================

**Tinysource has been merged with EXT:min.**

Bitbucket Repository: https://bitbucket.org/InstituteWeb/min
TYPO3 Extension Repository: https://typo3.org/extensions/repository/view/min/

**Please use this extension instead. Thanks.**


The tinysource extension compresses the frontend output output of TYPO3 CMS at the latest possible point,
before caches take effect.

Compression options:
--------------------

* **one line mode (enabled by default)**
* or separate what you want to remove from source code:
** strip tabs
** strip double spaces
** strip line breaks
** or reduce line breaks (converts more than two LF to one LF)
** comments (not removable in `<head>`)
* also you can keep search comments (`<!--TYPO3SEARCH_begin-->` and `<!--TYPO3SEARCH_end-->`)
* options can be different between `<head>` and `<body>` part
* supports nc_staticfilecache


Configuration
-------------

Tinysource includes the following configurations which make it highly customizable:

`plugin.tx_tinysource.`


.. container:: table-row

   Property
         enable

   Data type
         boolean (default: 1)

   Description
        En-/disables whole TinySource functionality.


.. container:: table-row

   Property
         oneLineMode

   Data type
         boolean (default: 1)

   Description
         *Since 5.0:* When set there are no line breaks separating html, head and body tags.
         But it requires the previous settings to be enabled to compress the source parts.



**Head**

.. container:: table-row

   Property
         head.stripTabs

   Data type
         boolean (default: 0)

   Description
         Strips all tabs inside of <head>

.. container:: table-row

   Property
         head.stripNewLines

   Data type
         boolean (default: 0)

   Description
         Strips all new lines (LF/CRLF) inside of <head>

.. container:: table-row

   Property
         head.stripDoubleSpaces

   Data type
         boolean (default: 1)

   Description
         Reduces more than one, sequenced spaces to one inside of <head>

.. container:: table-row

   Property
         head.stripTwoLinesToOne

   Data type
         boolean (default: 1)

   Description
         Reduces more than two, sequenced new lines to two inside of <head>. This has no effect if head.stripNewLines is enabled.



**Body**

.. container:: table-row

   Property
         body.stripComments

   Data type
         boolean (default: 1)

   Description
         Strips all html comments inside of <body>

.. container:: table-row

   Property
         body.stripTabs

   Data type
         boolean (default: 0)

   Description
         Same like head.stripTabs, but takes effect in <body>

.. container:: table-row

   Property
         body.stripNewLines

   Data type
         boolean (default: 0)

   Description
         Same like head.stripNewLines, but takes effect in <body>

.. container:: table-row

   Property
         body.stripDoubleSpaces

   Data type
         boolean (default: 1)

   Description
         Same like head.stripDoubleSpaces, but takes effect in <body>

.. container:: table-row

   Property
         body.preventStripOfSearchComment

   Data type
         boolean (default: 1)

   Description
         Prevents replacement of the TYPO3 search comment



**Custom replacements**


.. container:: table-row

   Property
         customReplacements

   Data type
         cArray

   Description
         Numeric array of replacements (customReplacements.10, .20, ...). The following attributes need to be used
         in the objects of cArray. Example: `plugin.tx_tinysource.customReplacements.10.type`

.. container:: table-row

   Property
         type

   Data type
         string

   Description
         Type of replacement TYPO3 should perform. Possible values: `str_replace` or `preg_replace`.

.. container:: table-row

   Property
         search

   Data type
         string

   Description
         The string to search for, which tinysource will replace (if type is `str_replace`)


.. container:: table-row

   Property
         pattern

   Data type
         string

   Description
         The regular expression pattern to search for, which tinysource will replace (if type is `preg_replace`)


.. container:: table-row

   Property
         replace

   Data type
         string

   Description
         String or pattern to replace with.


Examples
--------

**Disable tinysource for development**

During development you don't want to work with the compressed code. You can add this condition to your TypoScript:

   [globalVar = TSFE : beUserLogin > 0] && [globalVar = GP:debug = 1]
      plugin.tx_tinysource.enable = 0
      config.linkVars := addToList(debug(1))
   [global]


This requires to be logged in backend and the GET-parameter `?debug=1` (first) or `&debug=1`. If you do not want to
loose the debug parameter when navigating through your project, you can add "debug" to `config.linkVars`.

You can alternatively target the Development application context if set up accordingly:

   [applicationContext = Development*]
      plugin.tx_tinysource.enable = 0
   [global]


**Custom Replacement Example**

plugin.tx_tinysource {
   customReplacements {
      # Replaces single word
      10 {
         type = str_replace
         search = Kontrabass
         replace = Klavier
      }
      # Replaces all vowels with the letter "u"
      20 {
         type = preg_replace
         pattern = /[aeiou]/i
         replace = u
      }
   }
}


Links
-----

* Issue Tracker: https://forge.typo3.org/projects/extension-tinysource/issues
* Source code: https://bitbucket.org/ArminVieweg/tinysource
